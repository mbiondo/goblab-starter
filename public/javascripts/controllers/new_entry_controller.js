var EditEntryController = require('./edit_entry_controller');

// inherit from edit controller
var NewEntryController = EditEntryController.extend();

module.exports = NewEntryController;

