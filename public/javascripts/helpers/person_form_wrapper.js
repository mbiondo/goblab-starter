Ember.EasyForm.Config.registerWrapper('personFormWrapper', {
  formClass: 'form-horizontal',
  errorClass: '',
  hintClass: 'help-block',
  labelClass: '',
  inputClass: 'form-control',
});
